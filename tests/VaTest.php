<?php
/**
 * Created by eaz.
 * Date: 09/01/19
 * Time: 14:54
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhpTest;


use Eaz\BcaPhp\Clients\VirtualAccount;

class VaTest extends BaseTest
{
    /** @var VirtualAccount */
    private static $va;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        static::$va = static::$client->virtualAccount();
        static::auth();
    }

    public function testGetInquiry()
    {
        $this->checkToken();
        $getInquiry = static::$va->getInquiryStatusPayment(static::$token, '10111', '201711101617000000700000000001');
        dump($getInquiry->getBody());
        $this->assertTrue($getInquiry->isSuccess());
    }
}