<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 16:15
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhpTest;


use Carbon\Carbon;
use Eaz\BcaPhp\Clients\Banking;
use Eaz\BcaPhp\Models\Banking\DomesticFundTransferRequest;
use Eaz\BcaPhp\Models\Banking\FundTransferRequest;
use Eaz\BcaPhp\Types\BeneficiaryCustResidence;
use Eaz\BcaPhp\Types\BeneficiaryCustType;

class BankingTest extends BaseTest
{
    /** @var Banking */
    private static $banking;
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        static::$banking = static::$client->banking();
        static::auth();
    }

    public function testGetBalance(){
        $this->checkToken();
        $getBalance = static::$banking->getAccountsBalance(static::$token, 'BCAAPI2016',
            ['0201245680', '0063001004','1111111']);
        dump($getBalance->getBody());
        $this->assertTrue($getBalance->isSuccess());
    }

    public function testGetAccountStatement() {
        $this->checkToken();
        $getAccountStatement = static::$banking->getAccountStatement(static::$token, 'BCAAPI2016',
            '0201245680', '2016-08-29', '2016-09-01');
        dump($getAccountStatement->getBody());
        $this->assertTrue($getAccountStatement->isSuccess());
    }

     public function testPostFundTransferBCA() {
        $this->checkToken();
        $request = new FundTransferRequest (
            'BCAAPI2016',
            '00000001',
            '12345/PO/2016',
            '0201245680',
            Carbon::now()->toDateString(),
            'IDR',
            '100000.00',
            '0201245681'
        );

        $postFundTransferBCA = static::$banking->postFundTransferBca(static::$token, $request);
        dump($postFundTransferBCA->getBody());
        $this->assertTrue($postFundTransferBCA->isSuccess());
    }

    public function testPostFundTransferDomestic() {
        $this->checkToken();
        $request = new DomesticFundTransferRequest(
            '00000001',
            '12345/PO/2016',
            Carbon::now()->toDateString(),
            '0201245680',
            '0201245681',
            'BRINIDJA',
            'Tester',
            '100000.00',
            'LLG',
            BeneficiaryCustType::PERSONAL,
            BeneficiaryCustResidence::RESIDENT,
            'IDR'
        );

        $postFundTransferDomestic = static::$banking->postFundTransferDomestic(
            static::$token, '95051', 'BCAAPI2016', $request
        );
        dump($postFundTransferDomestic->getBody());
        $this->assertTrue($postFundTransferDomestic->isSuccess());
    }
}