<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 12:02
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhpTest;


use Carbon\Carbon;
use Eaz\BcaPhp\BcaClient;
use Eaz\BcaPhp\BcaClientConfig;
use PHPUnit\Framework\TestCase;

class BaseTest extends TestCase
{
    /** @var BcaClient */
    protected static $client;
    protected static $token;
    protected static $token_time;
    protected static $token_expired;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        static::$client = new BcaClient(static::getConfig());
    }

    public function checkToken() {
        if(is_null(static::$token)) {
            echo "token is null\n";
            $this->assertTrue(false);
            exit;
        }
    }

    public static function auth() {
        $auth = static::$client->auth();
        dump($auth);
        $body = $auth->getBody();
        if($auth->isSuccess()) {
            static::$token = $body->access_token;
            static::$token_time = Carbon::now();
            static::$token_expired = $body->expires_in;
        } else {
            dump($body);
        }
    }

    public static function getConfig() {
        $config = new BcaClientConfig();
        $config->setUrl("https://sandbox.bca.co.id:443");
//        $config->setUrl("https://devapi.klikbca.com:443");
        $config->setApiKey("51761363-4020-4217-82d7-9a7fb2a7e4a1");
        $config->setSecret("b6e59bc9-dc9b-46f2-aabc-7eb385bd33b1");
        $config->setClientId("ccc0e786-c75f-4e02-9e61-33b7da3fe6b6");
        $config->setClientSecret("97c10e7c-ffd3-45d8-9897-b6559e1df8e5");
        $config->setDomain("www.abc.com");

        return $config;
    }

    public function testEnv(){
        static::auth();
        dump(static::$token);
    }
}