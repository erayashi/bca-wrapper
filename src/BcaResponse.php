<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 14:28
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp;



use stdClass;
use Unirest\Response;

class BcaResponse
{
    private static $httpCode;
    private static $body;
    private static $rawBody;
    private static $request;
    private static $isSuccess;
    private static $headers;

    /**
     * @param Response $response
     * @return BcaResponse
     */
    public static function init($response) {
        $code = $response->code;
        if($code >= 200 && $code < 300)
            return self::Success($response);
        else
            return self::Error($response);
    }

    /**
     * @param $response Response
     * @return BcaResponse
     */
    public static function Success($response) {
        static::$isSuccess = true;
        static::$httpCode = $response->code;
        static::$rawBody = $response->raw_body;
        static::$body = json_decode(static::$rawBody);
        static::$headers = $response->headers;
        static::$request = null;
        return new static();
    }

    /**
     * @param Response $response
     * @return BcaResponse
     */
    public static function Error($response = null) {
        static::$isSuccess = false;

        if(!is_null($response)) {
            static::$rawBody = $response->raw_body;
            static::$httpCode = $response->code;
            static::$body = json_decode(static::$rawBody);
            static::$request = null;
        } else {
            static::$httpCode = 500;
            static::$rawBody = null;
            static::$body = new stdClass();
            static::$request = null;
        }

        return new static();
    }

    public function isSuccess()
    {
        return static::$isSuccess;
    }

    /**
     * @return mixed
     */
    public function getHttpCode()
    {
        return static::$httpCode;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return static::$body;
    }

    /**
     * @return mixed
     */
    public function getRawBody()
    {
        return static::$rawBody;
    }

    public function getHeaders()
    {
        return static::$headers;
    }
}
