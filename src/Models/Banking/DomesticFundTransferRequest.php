<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:48
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models\Banking;


use Eaz\BcaPhp\Models\ReferenceIDTrait;
use Eaz\BcaPhp\Models\TransactionIDTrait;

class DomesticFundTransferRequest
{
    use TransactionIDTrait, ReferenceIDTrait;
    public $TransactionDate;
    public $SourceAccountNumber;
    public $BeneficiaryAccountNumber;
    public $BeneficiaryBankCode;
    public $BeneficiaryName;
    public $Amount;
    public $TransferType;
    public $BeneficiaryCustType;
    public $BeneficiaryCustResidence;
    public $CurrencyCode;
    public $Remark1;
    public $Remark2;

    /**
     * DomesticFundTransferRequest constructor.
     * @param $TransactionID
     * @param $ReferenceID
     * @param $TransactionDate
     * @param $SourceAccountNumber
     * @param $BeneficiaryAccountNumber
     * @param $BeneficiaryBankCode
     * @param $BeneficiaryName
     * @param $Amount
     * @param $TransferType
     * @param $BeneficiaryCustType
     * @param $BeneficiaryCustResidence
     * @param $CurrencyCode
     * @param $Remark1
     * @param $Remark2
     */
    public function __construct($TransactionID, $ReferenceID, $TransactionDate, $SourceAccountNumber,
                                $BeneficiaryAccountNumber, $BeneficiaryBankCode, $BeneficiaryName, $Amount,
                                $TransferType, $BeneficiaryCustType, $BeneficiaryCustResidence, $CurrencyCode,
                                $Remark1 = null, $Remark2 = null)
    {
        $this->TransactionID = $TransactionID;
        $this->ReferenceID = $ReferenceID;
        $this->TransactionDate = $TransactionDate;
        $this->SourceAccountNumber = $SourceAccountNumber;
        $this->BeneficiaryAccountNumber = $BeneficiaryAccountNumber;
        $this->BeneficiaryBankCode = $BeneficiaryBankCode;
        $this->BeneficiaryName = $BeneficiaryName;
        $this->Amount = $Amount;
        $this->TransferType = $TransferType;
        $this->BeneficiaryCustType = $BeneficiaryCustType;
        $this->BeneficiaryCustResidence = $BeneficiaryCustResidence;
        $this->CurrencyCode = $CurrencyCode;
        $this->Remark1 = $Remark1;
        $this->Remark2 = $Remark2;
    }


    /**
     * @param mixed $TransactionDate
     */
    public function setTransactionDate($TransactionDate)
    {
        $this->TransactionDate = $TransactionDate;
    }

    /**
     * @param mixed $SourceAccountNumber
     */
    public function setSourceAccountNumber($SourceAccountNumber)
    {
        $this->SourceAccountNumber = $SourceAccountNumber;
    }

    /**
     * @param mixed $BeneficiaryAccountNumber
     */
    public function setBeneficiaryAccountNumber($BeneficiaryAccountNumber)
    {
        $this->BeneficiaryAccountNumber = $BeneficiaryAccountNumber;
    }

    /**
     * @param mixed $BeneficiaryBankCode
     */
    public function setBeneficiaryBankCode($BeneficiaryBankCode)
    {
        $this->BeneficiaryBankCode = $BeneficiaryBankCode;
    }

    /**
     * @param mixed $BeneficiaryName
     */
    public function setBeneficiaryName($BeneficiaryName)
    {
        $this->BeneficiaryName = $BeneficiaryName;
    }

    /**
     * @param mixed $Amount
     */
    public function setAmount($Amount)
    {
        $this->Amount = $Amount;
    }

    /**
     * @param mixed $TransferType
     */
    public function setTransferType($TransferType)
    {
        $this->TransferType = $TransferType;
    }

    /**
     * @param mixed $BeneficiaryCustType
     */
    public function setBeneficiaryCustType($BeneficiaryCustType)
    {
        $this->BeneficiaryCustType = $BeneficiaryCustType;
    }

    /**
     * @param mixed $BeneficiaryCustResidence
     */
    public function setBeneficiaryCustResidence($BeneficiaryCustResidence)
    {
        $this->BeneficiaryCustResidence = $BeneficiaryCustResidence;
    }

    /**
     * @param mixed $CurrencyCode
     */
    public function setCurrencyCode($CurrencyCode)
    {
        $this->CurrencyCode = $CurrencyCode;
    }

    /**
     * @param mixed $Remark1
     */
    public function setRemark1($Remark1)
    {
        $this->Remark1 = $Remark1;
    }

    /**
     * @param mixed $Remark2
     */
    public function setRemark2($Remark2)
    {
        $this->Remark2 = $Remark2;
    }
}