<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:33
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models\Banking;


use Eaz\BcaPhp\Models\CorporateIDTrait;
use Eaz\BcaPhp\Models\ReferenceIDTrait;
use Eaz\BcaPhp\Models\TransactionIDTrait;

class FundTransferRequest
{
    use CorporateIDTrait, ReferenceIDTrait, TransactionIDTrait;
    public $SourceAccountNumber;
    public $TransactionDate;
    public $CurrencyCode;
    public $Amount;
    public $BeneficiaryAccountNumber;
    public $Remark1;
    public $Remark2;

    /**
     * FundTransferRequest constructor.
     * @param $CorporateID
     * @param $TransactionID
     * @param $ReferenceID
     * @param $SourceAccountNumber
     * @param $TransactionDate
     * @param $CurrencyCode
     * @param $Amount
     * @param $BeneficiaryAccountNumber
     * @param $Remark1
     * @param $Remark2
     */
    public function __construct($CorporateID, $TransactionID, $ReferenceID, $SourceAccountNumber, $TransactionDate,
                                $CurrencyCode, $Amount, $BeneficiaryAccountNumber, $Remark1 = null, $Remark2 = null)
    {
        $this->CorporateID = $CorporateID;
        $this->TransactionID = $TransactionID;
        $this->ReferenceID = $ReferenceID;
        $this->SourceAccountNumber = $SourceAccountNumber;
        $this->TransactionDate = $TransactionDate;
        $this->CurrencyCode = $CurrencyCode;
        $this->Amount = $Amount;
        $this->BeneficiaryAccountNumber = $BeneficiaryAccountNumber;
        $this->Remark1 = $Remark1;
        $this->Remark2 = $Remark2;
    }


    /**
     * @return mixed
     */
    public function getSourceAccountNumber()
    {
        return $this->SourceAccountNumber;
    }

    /**
     * @param mixed $SourceAccountNumber
     */
    public function setSourceAccountNumber($SourceAccountNumber)
    {
        $this->SourceAccountNumber = $SourceAccountNumber;
    }

    /**
     * @return mixed
     */
    public function getTransactionDate()
    {
        return $this->TransactionDate;
    }

    /**
     * @param mixed $TransactionDate
     */
    public function setTransactionDate($TransactionDate)
    {
        $this->TransactionDate = $TransactionDate;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->CurrencyCode;
    }

    /**
     * @param mixed $CurrencyCode
     */
    public function setCurrencyCode($CurrencyCode)
    {
        $this->CurrencyCode = $CurrencyCode;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->Amount;
    }

    /**
     * @param mixed $Amount
     */
    public function setAmount($Amount)
    {
        $this->Amount = $Amount;
    }

    /**
     * @return mixed
     */
    public function getBeneficiaryAccountNumber()
    {
        return $this->BeneficiaryAccountNumber;
    }

    /**
     * @param mixed $BeneficiaryAccountNumber
     */
    public function setBeneficiaryAccountNumber($BeneficiaryAccountNumber)
    {
        $this->BeneficiaryAccountNumber = $BeneficiaryAccountNumber;
    }

    /**
     * @return mixed
     */
    public function getRemark1()
    {
        return $this->Remark1;
    }

    /**
     * @param mixed $Remark1
     */
    public function setRemark1($Remark1)
    {
        $this->Remark1 = $Remark1;
    }

    /**
     * @return mixed
     */
    public function getRemark2()
    {
        return $this->Remark2;
    }

    /**
     * @param mixed $Remark2
     */
    public function setRemark2($Remark2)
    {
        $this->Remark2 = $Remark2;
    }
}