<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:40
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models;


trait ReferenceIDTrait
{
    public $ReferenceID;

    /**
     * @return mixed
     */
    public function getReferenceID()
    {
        return $this->ReferenceID;
    }

    /**
     * @param mixed $ReferenceID
     */
    public function setReferenceID($ReferenceID)
    {
        $this->ReferenceID = $ReferenceID;
    }


}