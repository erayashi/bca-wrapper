<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 16:12
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models;


trait PaymentIDTrait
{
    public $PaymentID;

    /**
     * @return mixed
     */
    public function getPaymentID()
    {
        return $this->PaymentID;
    }

    /**
     * @param mixed $PaymentID
     */
    public function setPaymentID($PaymentID)
    {
        $this->PaymentID = $PaymentID;
    }
}