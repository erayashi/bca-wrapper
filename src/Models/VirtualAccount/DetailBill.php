<?php
/**
 * Created by eaz.
 * Date: 01/03/19
 * Time: 14.47
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models\VirtualAccount;


class DetailBill
{
    /** @var int */
    public $BillAmount;
    /** @var string */
    public $BillNumber;
    /** @var string */
    public $BillSubCompany;
    /** @var int */
    public $BillReference;

    /**
     * @param int $BillAmount
     */
    public function setBillAmount($BillAmount)
    {
        $this->BillAmount = $BillAmount;
    }

    /**
     * @param string $BillNumber
     */
    public function setBillNumber($BillNumber)
    {
        $this->BillNumber = $BillNumber;
    }

    /**
     * @param string $BillSubCompany
     */
    public function setBillSubCompany($BillSubCompany)
    {
        $this->BillSubCompany = $BillSubCompany;
    }

    /**
     * @param int $BillReference
     */
    public function setBillReference($BillReference)
    {
        $this->BillReference = $BillReference;
    }
}