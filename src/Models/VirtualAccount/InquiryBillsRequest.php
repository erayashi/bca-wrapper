<?php
/**
 * Created by eaz.
 * Date: 01/03/19
 * Time: 14.34
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models\VirtualAccount;


use Eaz\BcaPhp\Types\ChannelType;

class InquiryBillsRequest
{
    /** @var int */
    public $CompanyCode;
    /** @var string */
    public $CustomerNumber;
    /** @var string */
    public $RequestID;
    /** @var ChannelType */
    public $ChannelType;
    /** @var string */
    public $TransactionDate;
    /** @var string */
    public $AdditionalData;

    /**
     * @param int $CompanyCode
     */
    public function setCompanyCode($CompanyCode)
    {
        $this->CompanyCode = $CompanyCode;
    }

    /**
     * @param string $CustomerNumber
     */
    public function setCustomerNumber($CustomerNumber)
    {
        $this->CustomerNumber = $CustomerNumber;
    }

    /**
     * @param string $RequestID
     */
    public function setRequestID($RequestID)
    {
        $this->RequestID = $RequestID;
    }

    /**
     * @param ChannelType $ChannelType
     */
    public function setChannelType($ChannelType)
    {
        $this->ChannelType = $ChannelType;
    }

    /**
     * @param string $TransactionDate
     */
    public function setTransactionDate($TransactionDate)
    {
        $this->TransactionDate = $TransactionDate;
    }

    /**
     * @param string $AdditionalData
     */
    public function setAdditionalData($AdditionalData)
    {
        $this->AdditionalData = $AdditionalData;
    }
}