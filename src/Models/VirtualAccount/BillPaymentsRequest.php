<?php
/**
 * Created by eaz.
 * Date: 01/03/19
 * Time: 14.45
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models\VirtualAccount;


class BillPaymentsRequest
{
    /** @var string */
    public $CompanyCode;
    /** @var string */
    public $CustomerNumber;
    /** @var string */
    public $RequestID;
    /** @var string */
    public $ChannelType;
    /** @var string */
    public $CustomerName;
    /** @var string */
    public $CurrencyCode;
    /** @var int */
    public $PaidAmount;
    /** @var int */
    public $TotalAmount;
    /** @var string */
    public $SubCompany;
    /** @var string */
    public $TransactionDate;
    /** @var string */
    public $Reference;
    /** @var DetailBill[] */
    public $DetailBills;
    /** @var string */
    public $FlagAdvice;
    /** @var string */
    public $AdditionalData;

    /**
     * @param string $CompanyCode
     */
    public function setCompanyCode($CompanyCode)
    {
        $this->CompanyCode = $CompanyCode;
    }

    /**
     * @param string $CustomerNumber
     */
    public function setCustomerNumber($CustomerNumber)
    {
        $this->CustomerNumber = $CustomerNumber;
    }

    /**
     * @param string $RequestID
     */
    public function setRequestID($RequestID)
    {
        $this->RequestID = $RequestID;
    }

    /**
     * @param string $ChannelType
     */
    public function setChannelType($ChannelType)
    {
        $this->ChannelType = $ChannelType;
    }

    /**
     * @param string $CustomerName
     */
    public function setCustomerName($CustomerName)
    {
        $this->CustomerName = $CustomerName;
    }

    /**
     * @param string $CurrencyCode
     */
    public function setCurrencyCode($CurrencyCode)
    {
        $this->CurrencyCode = $CurrencyCode;
    }

    /**
     * @param int $PaidAmount
     */
    public function setPaidAmount($PaidAmount)
    {
        $this->PaidAmount = $PaidAmount;
    }

    /**
     * @param int $TotalAmount
     */
    public function setTotalAmount($TotalAmount)
    {
        $this->TotalAmount = $TotalAmount;
    }

    /**
     * @param string $SubCompany
     */
    public function setSubCompany($SubCompany)
    {
        $this->SubCompany = $SubCompany;
    }

    /**
     * @param string $TransactionDate
     */
    public function setTransactionDate($TransactionDate)
    {
        $this->TransactionDate = $TransactionDate;
    }

    /**
     * @param string $Reference
     */
    public function setReference($Reference)
    {
        $this->Reference = $Reference;
    }

    /**
     * @param DetailBill[] $DetailBills
     */
    public function setDetailBills($DetailBills)
    {
        $this->DetailBills = $DetailBills;
    }

    /**
     * @param string $FlagAdvice
     */
    public function setFlagAdvice($FlagAdvice)
    {
        $this->FlagAdvice = $FlagAdvice;
    }

    /**
     * @param string $AdditionalData
     */
    public function setAdditionalData($AdditionalData)
    {
        $this->AdditionalData = $AdditionalData;
    }
}