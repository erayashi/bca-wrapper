<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:39
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models;


trait TransactionIDTrait
{
    public $TransactionID;

    /**
     * @return mixed
     */
    public function getTransactionID()
    {
        return $this->TransactionID;
    }

    /**
     * @param mixed $TransactionID
     */
    public function setTransactionID($TransactionID)
    {
        $this->TransactionID = $TransactionID;
    }
}