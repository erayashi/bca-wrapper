<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 16:08
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models\Sakuku;


use Eaz\BcaPhp\Models\MerchantIDTrait;
use Eaz\BcaPhp\Models\ReferenceIDTrait;
use Eaz\BcaPhp\Models\TransactionIDTrait;

class CreatePayment
{
    use MerchantIDTrait, TransactionIDTrait, ReferenceIDTrait;
    public $MerchantName;
    public $RequestDate;
    public $Amount;
    public $CurrencyCode;
    public $Tax;
    public $Description;
    public $CallbackURL;

    /**
     * CreatePayment constructor.
     * @param $MerchantName
     * @param $RequestDate
     * @param $Amount
     * @param $CurrencyCode
     * @param $Tax
     * @param $Description
     * @param $CallbackURL
     */
    public function __construct($MerchantID, $TransactionID, $ReferenceID, $MerchantName, $RequestDate, $Amount,
                                $CurrencyCode, $Tax, $Description, $CallbackURL)
    {
        $this->MerchantID = $MerchantID;
        $this->TransactionID = $TransactionID;
        $this->ReferenceID = $ReferenceID;
        $this->MerchantName = $MerchantName;
        $this->RequestDate = $RequestDate;
        $this->Amount = $Amount;
        $this->CurrencyCode = $CurrencyCode;
        $this->Tax = $Tax;
        $this->Description = $Description;
        $this->CallbackURL = $CallbackURL;
    }

    /**
     * @return mixed
     */
    public function getMerchantName()
    {
        return $this->MerchantName;
    }

    /**
     * @param mixed $MerchantName
     */
    public function setMerchantName($MerchantName)
    {
        $this->MerchantName = $MerchantName;
    }

    /**
     * @return mixed
     */
    public function getRequestDate()
    {
        return $this->RequestDate;
    }

    /**
     * @param mixed $RequestDate
     */
    public function setRequestDate($RequestDate)
    {
        $this->RequestDate = $RequestDate;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->Amount;
    }

    /**
     * @param mixed $Amount
     */
    public function setAmount($Amount)
    {
        $this->Amount = $Amount;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->CurrencyCode;
    }

    /**
     * @param mixed $CurrencyCode
     */
    public function setCurrencyCode($CurrencyCode)
    {
        $this->CurrencyCode = $CurrencyCode;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->Tax;
    }

    /**
     * @param mixed $Tax
     */
    public function setTax($Tax)
    {
        $this->Tax = $Tax;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param mixed $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return mixed
     */
    public function getCallbackURL()
    {
        return $this->CallbackURL;
    }

    /**
     * @param mixed $CallbackURL
     */
    public function setCallbackURL($CallbackURL)
    {
        $this->CallbackURL = $CallbackURL;
    }
}