<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 16:12
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models\Sakuku;


use Eaz\BcaPhp\Models\MerchantIDTrait;
use Eaz\BcaPhp\Models\PaymentIDTrait;

class PaymentStatus
{
    use MerchantIDTrait, PaymentIDTrait;

    /**
     * PaymentStatus constructor.
     */
    public function __construct($MerchantID, $PaymentID)
    {
        $this->MerchantID = $MerchantID;
        $this->PaymentID = $PaymentID;
    }
}