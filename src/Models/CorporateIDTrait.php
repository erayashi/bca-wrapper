<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:39
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models;


trait CorporateIDTrait
{
    public $CorporateID;

    /**
     * @return mixed
     */
    public function getCorporateID()
    {
        return $this->CorporateID;
    }

    /**
     * @param mixed $CorporateID
     */
    public function setCorporateID($CorporateID)
    {
        $this->CorporateID = $CorporateID;
    }


}