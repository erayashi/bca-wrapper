<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 16:08
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Models;


trait MerchantIDTrait
{
    public $MerchantID;

    /**
     * @return mixed
     */
    public function getMerchantID()
    {
        return $this->MerchantID;
    }

    /**
     * @param mixed $MerchantID
     */
    public function setMerchantID($MerchantID)
    {
        $this->MerchantID = $MerchantID;
    }
}