<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:08
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Clients;


use Eaz\BcaPhp\BcaClient;

class ABaseClient
{
    protected $client;

    public function __construct(BcaClient $client)
    {
        $this->client = $client;
    }

    protected function modifiedUrl($url, $data){
        $templates = array_map(function ($template){
            return "{".$template."}";
        }, array_keys($data));
        $replaces = array_values($data);
        return str_replace($templates, $replaces, $url);
    }
}