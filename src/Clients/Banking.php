<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 11:37
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Clients;


use Eaz\BcaPhp\Models\Banking\DomesticFundTransferRequest;
use Eaz\BcaPhp\Models\Banking\FundTransferRequest;
use Unirest\Method;

class Banking extends ABaseClient
{
    const GET_ACCOUNTS_BALANCE = '/banking/v3/corporates/{corporate_id}/accounts/{account_numbers}';
    const GET_ACCOUNT_STATEMENT = '/banking/v3/corporates/{corporate_id}/accounts/{account_number}/statements?EndDate={end_date}&StartDate={start_date}';
    const POST_FUND_TRANSFER_BCA = '/banking/corporates/transfers';
    const POST_FUND_TRANSFER_DOMESTIC = '/banking/corporates/transfers/domestic';

    /**
     * @param $accessToken
     * @param $corporateId
     * @param array|string $accountNumbers
     * @return \Eaz\BcaPhp\BcaResponse
     */
    public function getAccountsBalance($accessToken, $corporateId, $accountNumbers) {
        $accountNumbers = is_array($accountNumbers) ? urlencode(implode(",", $accountNumbers)) : $accountNumbers;
        $data = [
            'corporate_id'      => $corporateId,
            'account_numbers'   => $accountNumbers
        ];
        $url = $this->modifiedUrl(self::GET_ACCOUNTS_BALANCE, $data);

        return $this->client->send(Method::GET, $url, $accessToken);
    }

    /**
     * @param $accessToken
     * @param $corporateId
     * @param $accountNumber
     * @param $startDate
     * @param $endDate
     * @return \Eaz\BcaPhp\BcaResponse
     */
    public function getAccountStatement($accessToken, $corporateId, $accountNumber, $startDate, $endDate) {
        $data = [
            'corporate_id'      => $corporateId,
            'account_number'    => $accountNumber,
            'start_date'        => $startDate,
            'end_date'          => $endDate
        ];
        $url = $this->modifiedUrl(self::GET_ACCOUNT_STATEMENT, $data);
        return $this->client->send(Method::GET, $url, $accessToken);
    }

    /**
     * @param $accessToken
     * @param FundTransferRequest $fundTransferRequest
     * @return \Eaz\BcaPhp\BcaResponse
     */
    public function postFundTransferBca($accessToken, $fundTransferRequest) {
        return $this->client->send(Method::POST, self::POST_FUND_TRANSFER_BCA, $accessToken,
            $fundTransferRequest);
    }

    /**
     * @param $accessToken
     * @param $channelID
     * @param $credentialID
     * @param DomesticFundTransferRequest $domesticFundTransferRequest
     * @return \Eaz\BcaPhp\BcaResponse
     */
    public function postFundTransferDomestic($accessToken, $channelID, $credentialID, $domesticFundTransferRequest) {
        $headers = [
            'ChannelID' => $channelID,
            'CredentialID' => $credentialID
        ];
        return $this->client->send(Method::POST, self::POST_FUND_TRANSFER_DOMESTIC, $accessToken,
            $domesticFundTransferRequest, $headers);
    }
}