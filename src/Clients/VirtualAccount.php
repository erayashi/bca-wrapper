<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 11:38
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Clients;


use Eaz\BcaPhp\Models\VirtualAccount\BillPaymentsRequest;
use Eaz\BcaPhp\Models\VirtualAccount\InquiryBillsRequest;
use Unirest\Method;

class VirtualAccount extends ABaseClient
{
    const GET_INQUIRY_STATUS_PAYMENT = '/va/payments?CompanyCode={company_code}&RequestID={request_id}';
    const POST_BILLS = '/va/bills';
    const POST_PAYMENTS = '/va/payments';

    /**
     * @param $accessToken
     * @param $companyCode
     * @param $requestID
     * @return \Eaz\BcaPhp\BcaResponse
     */
    public function getInquiryStatusPayment($accessToken, $companyCode, $requestID) {
        $data = [
            'company_code'  => $companyCode,
            'request_id'    => $requestID
        ];
        $path = $this->modifiedUrl(self::GET_INQUIRY_STATUS_PAYMENT, $data);
        return $this->client->send(Method::GET, $path, $accessToken);
    }

    /**
     * @param $accessToken
     * @param InquiryBillsRequest|array|object $inquiryBills
     * @return \Eaz\BcaPhp\BcaResponse
     */
    public function postBills($accessToken, $inquiryBills) {
        return $this->client->send(Method::POST, self::POST_BILLS, $accessToken, $inquiryBills);
    }

    /**
     * @param $accessToken
     * @param BillPaymentsRequest|array|object $payments
     * @return \Eaz\BcaPhp\BcaResponse
     */
    public function postPayments($accessToken, $payments) {
        return $this->client->send(Method::POST, self::POST_PAYMENTS, $accessToken, $payments);
    }
}