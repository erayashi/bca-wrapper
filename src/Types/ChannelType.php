<?php
/**
 * Created by eaz.
 * Date: 01/03/19
 * Time: 14.36
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Types;


class ChannelType
{
    const TELLER_BRANCH     = '6010';
    const ATM               = '6011';
    const POS_EDC           = '6012';
    const AUTO_DEBIT        = '6013';
    const INTERNET_BANKING  = '6014';
    const KIOSK             = '6015';
    const PHONE_BANKING     = '6016';
    const MOBILE_BANKING    = '6017';
    const LLG               = '6018';
}