<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:52
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Types;


class BeneficiaryCustResidence
{
    const RESIDENT = 1;
    const NON_RESIDENT = 2;
}