<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 15:51
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp\Types;


class BeneficiaryCustType
{
    const PERSONAL = 1;
    const CORPORATE = 2;
    const GOVERNMENT = 3;
}