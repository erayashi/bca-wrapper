<?php
/**
 * Created by eaz.
 * Date: 08/01/19
 * Time: 11:19
 * Github: https://github.com/erry-az
 */

namespace Eaz\BcaPhp;


use Carbon\Carbon;
use Eaz\BcaPhp\Clients\Banking;
use Eaz\BcaPhp\Clients\VirtualAccount;
use Unirest\Exception;
use Unirest\Method;
use Unirest\Request;
use Unirest\Request\Body;

class BcaClient
{
    const AUTH_PATH = '/api/oauth/token';
    /** @var BcaClientConfig */
    protected $config;
    private $time;
    private $headers;
    private $query;

    public function __construct($config)
    {
        $this->config = $config;

        Request::curlOpts(array(
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSLVERSION => 6,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => $this->config->getTimeout() < 120 ? $this->config->getTimeout() : 30
        ));
    }

    private function generateSignature($method, $path, $accessToken, $body = null) {
        $body = is_null($body) ? "" : json_encode($body, JSON_UNESCAPED_SLASHES);
        $hashBody = hash('sha256', $body);

        $forHash = [$method, $path, $accessToken, $hashBody, $this->time];
        $stringToSign = implode(":", $forHash);
        $hashHmac = hash_hmac('sha256', $stringToSign, $this->config->getSecret(), false);
        return $hashHmac;
    }

    public function send($method, $path, $accessToken, $body = null, $headers = null) {
        $method = strtoupper($method);
        $this->init($method, $path, $accessToken, $body);
        if(!is_null($headers) && is_array($headers))
            $this->headers = array_merge($this->headers, $headers);

        try {
            if($method == Method::GET) {
                $body = $this->query;
            } elseif (!is_null($body)) {
                $body = Body::Json($body);
            }
            $request = Request::send($method, $this->config->getUrl() . $path, $body, $this->headers);
            return BcaResponse::init($request);
        } catch (Exception $e) {
            return BcaResponse::Error();
        }
    }

    private function init($method, $path, $accessToken, $body = null) {
        $this->generateIsoTime();

        $this->headers = [
            "Accept" => "application/json",
            "Content-Type" => "application/json",
            "Authorization" => "Bearer $accessToken",
            "X-BCA-Key" => $this->config->getApiKey(),
            "X-BCA-Timestamp" => $this->time,
            "X-BCA-Signature" => $this->generateSignature($method, $path, $accessToken, $body)
        ];
    }

    public function auth($clientId = null, $clientSecret = null) {
        $clientId = $clientId ?: $this->config->getClientId();
        $clientSecret = $clientSecret ?: $this->config->getClientSecret();
        $headers = [
            'Authorization' => "Basic " . base64_encode($clientId . ":" . $clientSecret),
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $body = Body::form(['grant_type' => 'client_credentials']);
        $response = Request::post($this->config->getUrl() . self::AUTH_PATH, $headers, $body);
        return BcaResponse::init($response);
    }

    /**
     * @param string $key
     * @param string $val
     */
    public function addQuery($key, $val)
    {
        $this->query[$key] = $val;
    }

    /**
     * @param array $queries
     */
    public function addQueries(array $queries)
    {
        foreach ($queries as $key => $val) {
            $this->query[$key] = $val;
        }
    }

    public function generateIsoTime()
    {
        $date = Carbon::now($this->config->getTimezone());
        date_default_timezone_set($this->config->getTimezone());
        $fmt     = $date->format('Y-m-d\TH:i:s');
        $this->time = sprintf("$fmt.%s%s", substr(microtime(), 2, 3), date('P'));
    }

    public function banking() {
        return new Banking($this);
    }

    public function virtualAccount() {
        return new VirtualAccount($this);
    }
}